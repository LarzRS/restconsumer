import { LitElement, html, css } from 'lit-element';

class BookForm extends LitElement {
    static get styles() {
        return css`...`
    }

    static get properties() {
        return { 
            id: {type:Number},
            titulo: {type:String},
            autor: {type:String},
        };
    }

    constructor(){ 
        super();
        this.id = 0;
        this.titulo = "";
        this.autor = "";
    }

    render() {
        return html`
            <div>
                <label for="iId">ID</label>
                <input type="number" id="iId" .value="${this.id}" @input="${this.updateId}" />
                <br/>
                <label for="iTitulo">Titulo</label>
                <input type="text" id="iTitulo" .value="${this.titulo}" @input="${this.updateTitulo}" />
                <br/>
                <label for="iAutor">Autor</label>
                <input type="text" id="iAutor" .value="${this.autor}" @input="${this.updateAutor}" />
                <br/>
                <button @click="${this.buscarBook}">Buscar</button>
                <button @click="${this.crearBook}">Crear</button>
                <button @click="${this.modificarBook}">Modificar</button>
                <button @click="${this.eliminarBook}">Eliminar</button>
            </div>
        `
    }

    getCurrentBook() {
        var book = {};
        book.id = this.id;
        book.titulo = this.titulo;
        book.autor = this.autor;
        console.log(book);
        return book;
    }

    crearBook() {
        var book = this.getCurrentBook();
        const options = {
            method: "POST",
            body: JSON.stringify(book),
            headers: { "Content-Type": "application/json" }
        }

        fetch("http://localhost:3392/books", options)
        .then(response => {
            console.log(response);
            if(!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            alert("Book creado");
        })
        .catch(error => {
            alert("Problemas con el fetch", error);
        });
    }

    modificarBook() {
        var book = this.getCurrentBook();
        const options = {
            method: "PUT",
            body: JSON.stringify(book),
            headers: { "Content-Type": "application/json" }
        }

        fetch("http://localhost:3392/books", options)
        .then(response => {
            console.log(response);
            if(!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            alert("Book creado");
        })
        .catch(error => {
            alert("Problemas con el fetch", error);
        });
    }

    eliminarBook() {
        var book = this.getCurrentBook();
        const options = {
            method: "DELETE",
            body: "",
            headers: { "Content-Type": "application/json" }
        }

        fetch("http://localhost:3392/books/" + this.id)
        .then(response => {
            console.log(response);
            if(!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            alert("Book borrado");
        })
        .catch(error => {
            alert("Problemas con el fetch", error);
        });
    }

    buscarBook() {
        fetch("http://localhost:3392/books/" + this.id)
        .then(response => {
            console.log(response);
            if(!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            this.id = data.id;
            this.autor = data.autor;
            this.titulo = data.titulo;
            console.log(data);
        })
        .catch(error => {
            alert("Problemas con el fetch", error);
        });
    }

    updateId(e) {
        this.id = parseInt(e.target.value);
    }

    updateTitulo(e) {
        this.titulo = e.target.value;
    }

    updateAutor(e) {
        this.autor = e.target.value;
    }
}
customElements.define('book-form', BookForm);