import { LitElement, html, css } from 'lit-element';

class TestFetch extends LitElement {
    static get styles() {
        return css`...`
    }

    constructor() {
        super();
        this.planets = {results: []};
    }

    static get properties() {
        return { planets: [] };
    }

    render() {
        return html`
            ${this.planets.results.map((pl) => {
                return html`<div>${pl.name} ${pl.rotation_period}</div>`
            })}
        `
    }

    connectedCallback() {
        super.connectedCallback();
        try {
            this.cargarPlanetas();
        } catch (error) {
            alert(e)
        }
    }

    cargarPlanetas() {
        fetch("https://swapi.dev/api/planets")
        .then(response => {
            console.log(response);
            if(!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            this.planets = data;
            console.log(data);
        })
        .catch(error => {
            alert("Problemas con el fetch", error);
        });
    }
}
customElements.define('test-fetch', TestFetch);