import { LitElement, html, css } from 'lit-element';

class TestXmlhttps extends LitElement {
    static get styles() {
        return css`...`
    }

    constructor() {
        super();
        this.cargarPlaneta();
    }

    static get properties() {
        return { planet: {type:Object} };
    }

    render() {
        return html`<p><code>${this.planet}</code></p>`
    }

    cargarPlaneta() {
        // xmlhttprequest
        var req = new XMLHttpRequest();
        req.open("GET", "https://swapi.dev/api/planets/1");
        req.onreadystatechange = ((aEvt) => {
            if(req.readyState == 4) {
                if(req.status == 200) {
                    this.planet = req.responseText;
                    this.requestUpdate();
                } else {
                    alert("Error llamando rest");
                }
            }
        });
        req.send();
    }
}
customElements.define('test-xmlhttps', TestXmlhttps);